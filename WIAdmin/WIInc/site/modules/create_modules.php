<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  

     <style type="text/css">
       .mid {
    height: 250px;
}

.ui-widget-header {
    list-style: none;
}



.ui-draggable-handle {
    float: left;
    padding: 4px 7px 3px 0px;
    cursor: pointer;
    border: 1px solid #cf7cea;
}

.drop{
  border:2px dotted #0B85A1;
}


.column_drop{
      min-height: 50px;
    border: 1px solid green;
}


#droppable1:after {
    border: 1px solid #DDDDDD;
    border-radius: 4px 0 4px 0;
    color: #9DA0A4;
    content: "Container";
    font-size: 12px;
    font-weight: bold;
    left: -1px;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
    
}

.coldrop:after {
    border: 1px solid #DDDDDD;
    border-radius: 4px 0 4px 0;
    color: #9DA0A4;
    content: "Column";
    font-size: 12px;
    font-weight: bold;
    left: -1px;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
}

.coldrop{
      border: 1px dashed #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    padding: 39px 19px 24px;
    position: relative;
}


#delete{
      font-size: 0.5em;
      opacity: 0.4;
}

.remove{
    width: 12%;
    /* margin-left: 223px; */
    /* margin-top: -79px; */
    position: absolute;
    left: 541px;
    top: -11px;
    /* height: 6px; */
}


.column{
margin-left: -50px;
    height: 60px;
}


#tabsMenu{
    width: 100%;
    float: right; 
}

.next-group{
    margin-left: 155px;
    margin-top: -37px;
}

#draggable{
    padding: 0px;
    margin: 0px;
    height: 276px;
    width: 100%;
    overflow-y: scroll;
}

.panelling{
    width: 37%;
    float: right;
}

.stageWrap{
        width: 65%;
        min-height: 315px;
    float: left;
}

.stageWrap {
    position: relative;
    float: left;
    width: calc(74% - 102px);
    box-sizing: border-box;
    transition: width .25s;
    margin-right: 5px;
}

.stage{
    width: 100% ;
    height: 550px; 
    padding: 0.5em; 
    float: left;
    margin: 0px 5px 10px 0;
        background-color: #cfcfd4;
}

.site-wrap{
    margin-left: -25px;
}

.panell{
      width: 50%;
    padding: 0;
    margin: 0px 0px 0px 0px;
    height: 35px;
    float: left;  
}

.elementsC{
    font-size: 13px;
    text-align: -webkit-center;
    margin: 8px 0px 0px 0px;
}

.elementsE{
    font-size: 13px;
    text-align: -webkit-center;
    margin: 8px 0px 0px 0px;
}

.elementsL{
    font-size: 13px;
    text-align: -webkit-center;
    margin: 8px 0px 0px 0px;
}




     </style>
      <!--  -->


  
            <div class="row">

<div class="col-md-12 col-sm-12 col-lg-12">
              <div class="content">

  <link rel="stylesheet" type="text/css" href="WIInc/css/pagebuilder.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


  <div class="site-wrap">

    <section id="main_content" class="inner">
      <form class="build-form clearfix">
        <div class="stageWrap" id="stagearea"> 
            <ul class = "stage drop stage-empty"id="stage"></ul>
        </div>
         
          <div class="panelling">
            <div class="panel-wrap">
         

<div id="tabsMenu">

          <button class="prev-group" onclick="WIPageBuilder.RotateX()" title="Prevous Group" type="button" data-toggle="tooltip" data-placement="top"><img src="WIMedia/Img/mfp-left.png"></button>
  <ul class="panell">
    <li><a href="javascript:void(0)" class="elementsC on">Common Fields</a></li>
    <li><a href="javascript:void(0)" aria-hidden="true" class="elementsE off">HTML Elements</a></li>
    <li><a href="javascript:void(0)" aria-hidden="true" class="elementsL off">Layout</a></li>
  </ul>
   <button class="next-group" onclick="WIPageBuilder.Rotate()" title="Next Group" type="button" data-toggle="tooltip" data-placement="top"><img src="WIMedia/Img/mfp-right.png"></button>
  <div id="common" class="on">
    <?php  $mod->ActiveElementsCommonFields()  ?>
  </div>
  <div id="html" class="off">
    <?php  $mod->ActiveElementsHTMLElements()  ?>
  </div>
  <div id="layout" class="off">
        <?php  $mod->ActiveElementsLayouts()  ?>
  </div>
</div>

            </div>
            </div>
      </form>
      <div class="render-form"></div>
    </section>

  </div>

  
  <script src="WICore/WIJ/WIPageBuilder.js"></script>

                         <div class="col-md-9 col-lg-9 col-xs-9 cl-sm-9">
                        Module Name<input type="text" name="name" placeholder="Mod Name">
                        <button id="create_mod" class="col-md-9" onclick="WIMod.createMod()">Save Mod</button>
                        <div id="result"></div>

                        </div>

                    </div>
                </div>
            </div>


<script>
         $(function() {


             //
            //$("#draggable li")

            $('#draggable li').draggable({ 
              helper: 'clone',
              cursor: 'move',
              hoverClass: 'ui-state-active',
              revert: true
               
          });


           $("ul.drop").each(function(){
              $(this).droppable(
              {
               drop: function(e, ui) {
                const div = $(this).attr('id');
                const mod_name = ui.draggable.attr("id");
                console.log(div);
                console.log(mod_name);
                $("#tempStage").remove();
                WIMod.dropping(mod_name, div);
                
                $( this ).css('border', 'none');
               },
               over: function (e, ui) {
                  $( this ).css('border', '2px solid #0B85A1');
                
                $('ul.stage').append('<li class="stageRow" data-hover-tag="Row" data-editing-hover-tag="Editing Row" id="tempStage"></li>'); 
               },
               out: function (e, ui) {
                  $("#tempStage").remove();
                  $( this ).css('border', 'none');
               }

              });

            }) 


            

         });
      </script>

